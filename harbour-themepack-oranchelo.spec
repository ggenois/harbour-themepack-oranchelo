Name:          harbour-themepack-oranchelo
Version:       0.0.1
Release:       1
Summary:       Oranchelo icon pack
Group:         System/Tools
Vendor:        dfstorm
Distribution:  SailfishOS
Requires:      sailfish-version >= 2.0.1, harbour-themepacksupport >= 0.0.8-1
Packager:      dfstorm <dfstorm@riseup.net>
URL:           https://git.geno.is/root/harbour-themepack-oranchelo
License:       GPL

%description
Oranchelo icon pack for Sailfish OS.

%files
%defattr(-,root,root,-)
/usr/share/*

%postun
if [ $1 = 0 ]; then
    // Do stuff specific to uninstalls
rm -rf /usr/share/harbour-themepack-oranchelo
else
if [ $1 = 1 ]; then
    // Do stuff specific to upgrades
echo "Upgrading"
fi
fi

%changelog
* Thu Jul 12 2018 0.0.1
- First build.
